public class Tree{
    public static void main(String[] args) {
        int k = 10000;//определяем границу числа
        printArmstrongNumbers(k);
    }

    public static void printArmstrongNumbers(int k) {//метод перебирает метод от 1 до k
        for (int i = 1; i <= k; i++) {
            if (isArmstrongNumber(i)) {// вызываем вспомогательный метод
                System.out.println(i);
            }
        }
    }

    public static boolean isArmstrongNumber(int n) {//метод проверяет является ли число искомым
        String numStr = Integer.toString(n);
        int numDigits = numStr.length();
        int sum = 0;
        for (int i = 0; i < numDigits; i++) {
            int digit = Character.getNumericValue(numStr.charAt(i));
            sum += Math.pow(digit, numDigits);
        }
        return n == sum;
    }
}
