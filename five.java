import java.util.Arrays;

public class five {
    public static void main(String[] args) {
        int[] lock = {0, 0, 3, 5, 0, 3, 0, 5, 0, 0}; // начальное состояние
        int[] solution = solveLock(lock); // solve the lock
        System.out.println("Solution: " + Arrays.toString(solution));
    }

    public static int[] solveLock(int[] lock) {
        int[] solution = new int[10]; // массив для хранинея решения
        //перебор всех возможных комбинаций
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j <= 6; j++) {
                // прверяем сумму 3х ячеек
                if (lock[0] + lock[1] + i == 10) {
                    if (lock[1] + i + j == 10) {
                        if (i + j + lock[4] == 10) {
                            // хаполняем массив решения
                            solution[0] = lock[0];
                            solution[1] = lock[1];
                            solution[2] = i;
                            solution[3] = j;
                            solution[4] = lock[4];
                            // заполним оставшиеся ячейуи случайнми бросками
                            for (int k = 5; k < 10; k++) {
                                solution[k] = (int) (Math.random() * 6) + 1;
                            }
                            return solution;
                        }
                    }
                }
            }
        }

        return null; // no solution found
    }
}
