public class Four {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int sum1 = getSumOfConsecutiveNumbers(array, 2, 3);
        int sum2 = getSumOfConsecutiveNumbers(array, 4, 4);
        int sum3 = getSumOfConsecutiveNumbers(array, 7, 1);

        System.out.println(sum1);
        System.out.println(sum2);
        System.out.println(sum3);
    }

    public static int getSumOfConsecutiveNumbers(int[] array, int indexFrom, int countOfElements){
        int sum = 0;

        for (int i = indexFrom; i < indexFrom + countOfElements; i++) {
            sum += array[i];
        }

        return sum;
    }
}
