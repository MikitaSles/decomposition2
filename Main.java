public class Main {
    public static void main(String[] args) {
        int a = 8;//задаём три числа
        int b = 4;
        int c = 27;

        boolean areCoprime = areCoprime(a, b, c);//вызываем метод который вычеслит НОД
        System.out.printf("%d, %d, and %d are %s.\n", a, b, c, areCoprime ? "coprime" : "not coprime");
    }
    public static boolean areCoprime(int a, int b, int c) { // метод принимает 3 целых числа в качестве аргументов
        int gcd1 = gcd(a, b);//внутри метода используется метод "gcd" для нахождения НОД между парами чисел
        int gcd2 = gcd(gcd1, c);
        return gcd2 == 1;
    }

    public static int gcd(int a, int b) {// метод gcd реализует алгоритм Эйлера для нахождения наибольшего общего делителя
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);//в противном случае метод вызывает сам себя с аргументом b и остатком от деления a и b
    }
}